package jdbc.mysql_db_connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class MySqlConnection {
	static Connection connection = null;
	static PreparedStatement statement = null;
	
	public static void main (String[] args) {
		
		try {
			makeConnection();
			
			addDataToDB("EGS", "Abelyan 6/1", 400);
			addDataToDB("Synopsis", "Arshakunyats 41", 1000);
			getDataFromDB();
			
			statement.close();
			connection.close();
		} 
		catch(SQLException ex) {
			ex.printStackTrace();
		} 
	}
	
	private static void makeConnection() {
		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/companies", "root", "asdf1234");
			
			if (connection != null) {
				System.out.println("Connection Successful!!!");
			} else {
				System.out.println("Connection Failed.");
			}
		} catch(SQLException ex) {
			System.out.println("MySql connection failed");
			ex.printStackTrace();
			return;
		}
	}
	
	private static void addDataToDB (String companyName, String address, int employeeCount) {
		try {
			statement = connection.prepareStatement("INSERT INTO employee VALUES (?,?,?)");
			
			statement.setString(1, companyName);
			statement.setString(2, address);
			statement.setInt(3, employeeCount);
			
			statement.executeUpdate();
			
			System.out.println(companyName + " added successfuly");
		} 
		catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	private static void getDataFromDB () {
		try {
			statement = connection.prepareStatement("SELECT * FROM employee");
			
			ResultSet rs = statement.executeQuery();
			
			while (rs.next()) {
				String name = rs.getString("name");
				String address = rs.getString("address");
				int count = rs.getInt("employees");
				
				System.out.format("%s, %s, %s\n", name, address, count);
			}
		} 
		catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
}